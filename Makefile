install:
	npm install

dev:
	npm run dev

node_modules:
	# For use by the CI
	npm ci

build: node_modules
	npm run build

prod-preview:
	npm run preview

publish: build
	# This is called by a publish.sh script which contains the following line:
	# SRHT_SITE=username.srht.site SRHT_TOKEN=s3cr3t make publish
	# The token needs the scope pages.sr.ht:PAGES (read+write)
	cd dist && tar -cvz . > ../public.tar.gz
	curl --oauth2-bearer $$SRHT_TOKEN \
		-Fcontent=@public.tar.gz \
		https://pages.sr.ht/publish/$$SRHT_SITE
