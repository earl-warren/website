# Website for Forgejo

The website is build with [Astro](https://astro.build).

## Project Structure

```
/
├── public/
│   └── favicon.svg
├── src/
│   ├── components/
│   │   └── Card.astro
│   ├── layouts/
│   │   └── Layout.astro
│   └── pages/
│       ├── index.astro
│       └── code-of-conduct.md
└── package.json
```

Astro looks for `.astro` or `.md` files in the `src/pages/` directory. Each page is exposed as a route based on its file name, with a _trailing slash_.

There's nothing special about `src/components/`, but that's where we like to put any Astro/React/Vue/Svelte/Preact components.

Any static assets, like images, can be placed in the `public/` directory.

## Commands

All commands are run from the root of the project, from a terminal:

| Command                | Action                                             |
| :--------------------- | :------------------------------------------------- |
| `npm install`          | Installs dependencies                              |
| `npm run dev`          | Starts local dev server at `localhost:3000`        |
| `npm run build`        | Build your production site to `./dist/`            |
| `npm run preview`      | Preview your build locally, before deploying       |
| `npm run astro ...`    | Run CLI commands like `astro add`, `astro preview` |
| `npm run astro --help` | Get help using the Astro CLI                       |

## Styling

The styling is achieved using [Tailwind CSS](https://tailwindcss.com/). Custom classes can be added to `src/style.css`.

The standard layout is implemented in `src/layouts/Layout.astro` (see neighbour files for other layouts).

## Continuous deployment

The `main` branch triggers a Woodpecker build (see `.woodpecker.yml`), which pushes the "compiled" files to https://codeberg.org/forgejo/pages, which is accessible via https://forgejo.codeberg.page/.

The shared user https://codeberg.org/forgejo-website with email website@forgejo.org has write permissions to https://codeberg.org/forgejo/pages and the CI relies an application token from this user to push the pages.

## Want to learn more?

Feel free to check [the documentation of Astro](https://docs.astro.build).
