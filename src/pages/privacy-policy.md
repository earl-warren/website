---
layout: '@layouts/Markdown.astro'
---

Forgejo takes your privacy seriously. To better protect your privacy we provide this privacy policy notice explaining the way your personal information is collected and used.

#### Information we collect

This website does not track information about its visitors. We do however host this site on an external hosting provider. See [Other Third Parties](#other-third-parties) for details.

#### Your contributions

For all data you contribute (for example code, page content, people profile), you have full responsibility and control to add, modify and create this data. If some of your data cannot be modified by you, this is considered a technical bug that needs urgent fixing. Please report all issues to the Forgejo [Issue Tracker](https://codeberg.org/forgejo/meta/issues).

Contributions are subjected to a [Developer Certificate of Origin](https://developercertificate.org/).

#### Cookies and Ads

This website does not use cookies, hidden pixels or any other trackers. We certainly do not include ads anywhere on our website.

#### Other Third Parties

The website source is hosted on [Codeberg e.V.](https://join.codeberg.org/) under a minimum-collection policy. All site data is subjected to the [Codeberg Terms of Use](https://codeberg.org/Codeberg/org/src/branch/main/TermsOfUse.md). Please read the [Codeberg Privacy Policy](https://codeberg.org/codeberg/org/src/branch/main/PrivacyPolicy.md).

#### Links to Third Party Websites

We have included links on this website for your use and reference. Following these links may lead to the `REFERRER` URL to be added to their server logs. We are not responsible for the privacy policies on these websites. You should be aware that the privacy policies of these websites may differ from our own.

#### Security

The security of your personal information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage, is 100% secure. While we strive to use acceptable means to protect your personal information, we cannot guarantee its absolute security.

#### Changes to This Privacy Policy

This Privacy Policy is effective as of 2022 and will remain in effect except with respect to any changes in its provisions in the future, which will be in effect immediately after being posted on this page.

We reserve the right to update or change our Privacy Policy at any time and you should check this Privacy Policy periodically. If we make any material changes to this Privacy Policy, we will notify you by placing a prominent notice on our website for a period of time and document the change on our repository [README](https://codeberg.org/forgejo/website/src/branch/main/README.md).

#### Contact Information

You can reach out to us by [creating an issue](https://codeberg.org/forgejo/meta/issues) in our tracker.
