import type { Plugin } from 'unified';
import type { Root, Link } from 'mdast';

import { visit } from 'unist-util-visit';

/**
 * A remark plugin to prefix the URLs starting with "/" with a base.
 */
const prefixwithBase: Plugin<[String], Root> = function (base: String) {
	if (base.endsWith('/')) {
		base = base.substring(0, base.length - 1);
	}
	return (tree) => {
		visit(tree, 'link', (node: Link) => {
			if (!node.url || !node.url.startsWith('/')) {
				return;
			}
			node.url = base + node.url;
		});
	};
};

export default prefixwithBase;
