---
layout: '@layouts/Markdown.astro'
---

## What is Forgejo?

Forgejo is a self-hosted lightweight software forge, easy to install and low maintenance that just does the job.

A security team is available at `security@forgejo.org` (GPG public key [1B638BDF10969D627926B8D9F585D0F99E1FB56F](https://keyoxide.org/security@forgejo.org)) and is tasked to identify, define, and respond to vulnerabilities.

## What makes Forgejo unique?

Forgejo is designed to allow software forges to communicate with one another so that thousands of instances can be federated into a resilient and diverse network that scales horizontally. Other forges are designed to be centralized and scale up.

## Who is behind Forgejo?

The Forgejo domains (forgejo.org, forgejo.com, etc.) are under the stewardship of [Codeberg e.V.](https://docs.codeberg.org/getting-started/what-is-codeberg/#what-is-codeberg-e.v.%3F), a democratic non-profit organization dedicated to [hosting Free Software projects](https://codeberg.org/).

The people working on Forgejo are members of a [self-governed community](https://codeberg.org/forgejo/meta) upholding core values:

* Free Software
* Federation
* Transparency
* Diversity
* Democracy
* Privacy

## How is it different from Gitea?

Forgejo is a "soft" fork of Gitea with a focus on scaling, federation and privacy.
