import { defineConfig } from 'astro/config';
import tailwind from '@astrojs/tailwind';

import mdPrefixWithBase from './src/md-prefix-with-base';

const base = '/';

const remarkPlugins = [];
if (base != '/') {
	remarkPlugins.push([mdPrefixWithBase, base]);
}

// https://astro.build/config
export default defineConfig({
	site: 'https://forgejo.codeberg.page', // TODO: update with correct URL on prod (needed for for sitemap and canonical URLs final build)
	base,
	trailingSlash: 'always',
	integrations: [
		tailwind({
			config: {
				applyBaseStyles: false,
			},
		}),
	],
	markdown: {
		extendDefaultPlugins: true,
		remarkPlugins,
	},
});
